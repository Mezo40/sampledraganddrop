﻿namespace DragAndDrop
{
    partial class ui_frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ui_pnlDragContainer = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.xui_lblHeader = new System.Windows.Forms.Label();
            this.ui_pnlDragContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // ui_pnlDragContainer
            // 
            this.ui_pnlDragContainer.Controls.Add(this.button3);
            this.ui_pnlDragContainer.Controls.Add(this.button2);
            this.ui_pnlDragContainer.Controls.Add(this.button1);
            this.ui_pnlDragContainer.Location = new System.Drawing.Point(12, 146);
            this.ui_pnlDragContainer.Name = "ui_pnlDragContainer";
            this.ui_pnlDragContainer.Size = new System.Drawing.Size(776, 100);
            this.ui_pnlDragContainer.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(407, 14);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(68, 68);
            this.button3.TabIndex = 2;
            this.button3.Text = "Drag And Drop Me3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(333, 14);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(68, 68);
            this.button2.TabIndex = 1;
            this.button2.Text = "Drag And Drop Me2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(259, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 68);
            this.button1.TabIndex = 0;
            this.button1.Text = "Drag And Drop Me1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // xui_lblHeader
            // 
            this.xui_lblHeader.AutoSize = true;
            this.xui_lblHeader.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xui_lblHeader.ForeColor = System.Drawing.Color.DodgerBlue;
            this.xui_lblHeader.Location = new System.Drawing.Point(100, 61);
            this.xui_lblHeader.Name = "xui_lblHeader";
            this.xui_lblHeader.Size = new System.Drawing.Size(615, 36);
            this.xui_lblHeader.TabIndex = 1;
            this.xui_lblHeader.Text = "Drag and drop the buttons to see the effect";
            // 
            // ui_frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 387);
            this.Controls.Add(this.xui_lblHeader);
            this.Controls.Add(this.ui_pnlDragContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ui_frmMain";
            this.ShowIcon = false;
            this.Text = "Drag and Drop";
            this.ui_pnlDragContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel ui_pnlDragContainer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label xui_lblHeader;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
    }
}

