﻿/**
 * SEE LICENSE.md
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DragAndDrop
{
    public partial class ui_frmMain : Form
    {
        private readonly List<Button> m_panelButtons;
        private Point m_mousePoint;
        private Point m_primaryL;

        public ui_frmMain()
        {
            InitializeComponent();
            m_panelButtons = new List<Button>();

            InitializePanel();
        }

        private bool InArea(Rectangle area, Point p)
        {
            return area.Contains(p);
        }

        private void InitializePanel()
        {
            // Thanks for https://stackoverflow.com/questions/16384903/moving-a-control-by-dragging-it-with-the-mouse-in-c-sharp
            // On Ideas for Moving the control
            foreach (Control c in ui_pnlDragContainer.Controls)
            {
               if (c is Button b)
                {
                    b.MouseDown += (sender, e) =>
                    {
                        if (e.Button == MouseButtons.Left)
                        {
                            m_mousePoint = e.Location;
                            m_primaryL = (sender as Button).Location;
                        }
                    };
                    b.MouseMove += (sender, e) =>
                    {
                        if (e.Button == MouseButtons.Left)
                        {
                            (sender as Button).BringToFront();
                            (sender as Button).Left = e.X + (sender as Button).Left - m_mousePoint.X;
                        }
                    };
                    b.MouseUp += (sender, e) =>
                    {
                        if (e.Button == MouseButtons.Left)
                        {
                            foreach (Button btn in m_panelButtons)
                            {
                                if (!btn.Equals(sender))
                                {
                                    Button bn = sender as Button;
                                    Rectangle r = new Rectangle(btn.Location, btn.Size);
                                    if (r.Contains(bn.Location))
                                    {
                                        // Reorder the buttons
                                        bn.Location = btn.Location;
                                        btn.Location = m_primaryL;
                                    }
                                }
                            }
                        }
                    };
                    m_panelButtons.Add(b);
                }
            }
        }
    }
}
